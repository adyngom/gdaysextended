start with:
git-scm.com

Install git

Settings:

help.github.com/articles/set-up-git

git config --global user.name "User Name"

git config --global user.email me@me.com

git config --global color.ui true

git init  >>> initialize the directory

git status >>> get the state of the directory

git add >>> use it to add your latest changes

git add --all >>>>> will add all new or modified files

git commit -m "enter commit messge" >>> store our stage changes and add a message describing the changes

git commit --amend -m "message" >>>>> add to the last commit

git add '*.ext' >>>> use wildcard to add many files

git log >>> to get a complete picture of all the changes

git remote add origin http://url >>> add a remote url 

git push -u origin master >>>> name of remote is origin and local branch is master. 

git pull origin master >>> getting latest changes from origin 

git diff >>> to find out difference between files or folders

git reset >>> to remove the file from the staged area

git reset --soft HEAD^ >> undo last commit

git reset --hard HEAD^  >>>> undo last commit and all changes

git reset --hard HEAD^^ >>>>> undo last 2 commits and all changes

git checkout -- name_of_file >> get rid of all changes to file since last commit

git branch name_of_branch >>> to create a new branch

git checkout name_of_branch >>> to with to new branch

git rm >>> to remove files or folders

git merge name_of sibling_branch  >>> merges your changes from the sibling to this branch

git branch -d name_of_branch >>> when the branch is no longer needed use the -d flag to delete it